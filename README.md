Make New Repo
Name : Hamdani Abdullah
Repo Have 2 File

1. Answer.js -> Your Code
2. README.md -> Cases

Cases :

- create a function that accept two string parameter
- your function should be able to determine the first parameter is anagram with from the second parameter vice versa
- anagram will happen if the amount of each letter from first parameter is exactly equal with the second parameter and vice versa
- no googling / open repo / project
- no build in function (split, filter, sort, map, etc)
- examples:
  - `anagram('aaz', 'zza')` => false
  - `anagram('anagram', 'nagaram'))` => true
