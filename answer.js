function anagram(string1, string2) {
  const newString1 = string1.split("");
  const newString2 = string2.split("");
  let arrayString1 = [];

  for (let i = 0; i < newString1.length; i++) {
    for (let j = 0; j < newString2.length; j++) {
      if (newString1[i] === newString2[j]) {
        arrayString1.push(newString1[i]);
        delete newString1[i];
        delete newString2[j];
      }
    }
  }
  let finalString1 = arrayString1.join("");
  return finalString1 === string1;
}
console.log(anagram("aaz", "zza"));
console.log(anagram("anagram", "nagaram"));
